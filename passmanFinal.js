const R = require('ramda')
const readline = require('readline-sync')

let passwordsOfUser = {}
let userProfiles = {}

const addChangeCredentials = R.assocPath
const getPasswordOfAppWithEmail = R.path
const getCredsOfApp = function(nameOfApp, passwordObject){
    const valueOfEachProperty = (key) => [key, passwordObject[nameOfApp][key]]
    const getCredentials = R.pipe(
        R.path,
        R.keysIn,
        R.map(valueOfEachProperty),
        R.fromPairs
    )
    return getCredentials([nameOfApp], passwordObject)
}
const getAllPasswords = R.identity(passwordsOfUser)
const removeAllCreds = R.omit
const removeACred = R.dissocPath
const saveToProfile = function(userName, userPassword, passwordsObject, profilesObject){
    const save = R.pipe(
        R.assocPath([userName, 'passwords']),
        R.assocPath([userName, 'password'], userPassword)
    )
    return save(passwordsObject, profilesObject)
}
const getProfileData = function(userName, password, profileObject){
    const isEqualToPass = profile => R.path(["masterPass"], profile) === password
    const getMatch = R.filter(isEqualToPass)
    
    const allPasswordsFromProfile = R.pipe(
        getMatch,
        R.path([userName, 'passwords']),
    )
    return allPasswordsFromProfile(profileObject)
}



//input output system
let userInput = ""
console.clear()
while(userInput != "end"){
    let commandExecuteStatus = "succesful!"
    userInput = R.split(" ", (readline.question(">>> ")))
    console.clear()
    const commandMap = {
        "addto":function() {passwordsOfUser = addChangeCredentials([R.toLower(userInput[1]), userInput[2]], userInput[3], passwordsOfUser)},
        "savetoprofile": function() {userProfiles = saveToProfile(R.toLower(userInput[1]), userInput[2], passwordsOfUser, userProfiles)},
        "clear": function() {console.clear()}
    }
    console.log(userInput)
    console.log("----------------------------------------------")
    try {
        commandMap[R.toLower(userInput[0])]()
    } catch {
        commandExecuteStatus = "Unsuccessful. Command doesn't exist";
    }

    console.log("passwords of user")
    console.log(passwordsOfUser)
    console.log("\n\n\nuser profile data")
    console.log(userProfiles)
    console.log("----------------------------------------------\nStatus: " + commandExecuteStatus)
    console.log("----------------------------------------------")
}



// command list:
// addTo <appName> <email/username> <password>
// change <appName> <email/username> <password>
// - adds specified email and pass
// - if already exists, overwrites the 

// getPasswordFrom <appName> <email>
// - get password given the email of an app

// getAllFrom <appName>
// - get every credential under specified app

// getAll
// - prints everthing password saved onscreen

// deleteCredential <appName> <email/username>
// - deletes a single credential saved under an appName

// deleteAllFrom <appname>
// - deletes every credentials associated with the appname given

// have yet to have functions
// saveToProfile <profileUsername> <password>
// - saves all passwords to the supplied profile and locks with a password

// getFromProfile <profileUsername> <password>
// - gets password data from a profile and overwrites existing password data in memory 